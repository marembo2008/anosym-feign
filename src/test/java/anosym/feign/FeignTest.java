package anosym.feign;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.collect.ImmutableList;

import anosym.feign.paging.PagingStateInfo;
import anosym.tests.junit.CdiJUnitExtension;
import anosym.tests.wiremock.WiremockExtension;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 22, 2018, 12:40:25 PM
 */
@Slf4j
@ExtendWith(CdiJUnitExtension.class)
public class FeignTest {

    @RegisterExtension
    public static final WiremockExtension api = new WiremockExtension();

    @Inject
    private TestApiService apiService;

    @Inject
    private PageInfoApi pageInfoApi;

    @Inject
    private TestApi testApi;

    @BeforeEach
    public void setUp() {
        final var apiPort = System.getProperty(WiremockExtension.SERVER_PORT_PROPERTY);
        api.stubFor(get(urlPathEqualTo("/ids/1"))
                .willReturn(okJson("[\"test000\"]")));
        api.stubFor(post(urlPathEqualTo("/ids/4/redirect"))
                .willReturn(aResponse().withStatus(302)
                        .withHeader("location", "http://localhost:" + apiPort + "/ids/4")));
        api.stubFor(post(urlPathEqualTo("/ids/4"))
                .willReturn(okJson("[\"test00023\"]")));
        api.stubFor(get(urlPathEqualTo("/ids/2"))
                .willReturn(okJson("[\"test001\"]")));
        api.stubFor(get(urlPathMatching("/info.*"))
                .withQueryParam("page", WireMock.equalTo("98jk878j#10"))
                .willReturn(okJson("[\"page1\"]")));
    }

    @Test
    public void test_verifyMultipleInstancesForFeignId() {
        assertThat(apiService.getTestIds(), hasItems("test000", "test001"));

        //verify api calls
        api.verify(1, getRequestedFor(urlPathEqualTo("/ids/1")));
        api.verify(1, getRequestedFor(urlPathEqualTo("/ids/2")));
    }

    @Test
    public void test_verifyRedirect() {
        assertThat(testApi.getIdsWithRedirect(4), hasItems("test00023"));

        //verify api calls
        api.verify(1, postRequestedFor(urlPathEqualTo("/ids/4")));
        api.verify(1, postRequestedFor(urlPathEqualTo("/ids/4/redirect")));
    }

    @Test
    public void test_verifyParamExpander() {
        final PagingStateInfo page = PagingStateInfo.builder()
                .pageSize(10)
                .pagingState("98jk878j")
                .build();
        assertThat(pageInfoApi.getInfos(page), hasItem("page1"));
        api.verify(1, getRequestedFor(urlPathMatching("/info.*")));
    }

    @ApplicationScoped
    static class TestApiService {

        @Inject
        private TestApi test1Api;

        @NonNull
        public List<String> getTestIds() {
            return ImmutableList.<String>builder()
                    .addAll(test1Api.getIds(1))
                    .addAll(test1Api.getIds(2))
                    .build();
        }

    }

    @FeignClient("test")
    static interface TestApi {

        @RequestLine("GET /ids/{id}")
        @Headers("Accept: application/json")
        List<String> getIds(@Param("id") final int id);

        @RequestLine("POST /ids/{id}/redirect")
        @Headers("Accept: application/json")
        List<String> getIdsWithRedirect(@Param("id") final int id);

    }

    @FeignClient("page-info")
    static interface PageInfoApi {

        @RequestLine("GET /info/?page={page}")
        @Headers("Accept: application/json")
        List<String> getInfos(@Param("page") final PagingStateInfo page);

    }

}
