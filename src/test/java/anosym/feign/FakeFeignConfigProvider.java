package anosym.feign;

import anosym.feign.config.FeignConfigInstance;
import anosym.feign.config.FeignConfigProvider;
import anosym.feign.config.HttpConnectionConfig;
import anosym.feign.config.RedirectStrategyConfig;
import anosym.tests.wiremock.WiremockExtension;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Specializes;

/**
 *
 * @author ochieng-marembo
 */
@Specializes
@ApplicationScoped
public class FakeFeignConfigProvider extends FeignConfigProvider {

    @Override
    public FeignConfigInstance getFeignConfigInstance(String feignClientInstanceId) {
        return switch (feignClientInstanceId) {
            case "test" ->
                testConfig;
            case "page-info" ->
                apiInfoConfig;
            default ->
                throw new IllegalArgumentException("Unsupported api");
        };
    }

    private static final FeignConfigInstance testConfig = FeignConfigInstance.builder()
            .endpoint("http://localhost:" + System.getProperty(WiremockExtension.SERVER_PORT_PROPERTY))
            .httpsEnabled(false)
            .active(true)
            .httpConnectionConfig(new HttpConnectionConfig())
            .redirectStrategyConfig(new RedirectStrategyConfig(true, true))
            .build();

    private static final FeignConfigInstance apiInfoConfig = FeignConfigInstance.builder()
            .endpoint("http://localhost:" + System.getProperty(WiremockExtension.SERVER_PORT_PROPERTY))
            .httpsEnabled(false)
            .active(true)
            .httpConnectionConfig(new HttpConnectionConfig())
            .redirectStrategyConfig(new RedirectStrategyConfig(true, false))
            .build();
}
