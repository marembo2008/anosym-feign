package anosym.feign;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import jakarta.annotation.Nullable;

import anosym.feign.config.FeignConfigInstance;
import anosym.feign.config.HttpConnectionConfig;
import feign.Client;
import feign.Request;
import feign.Response;
import feign.httpclient.ApacheHttpClient;
import jakarta.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 30, 2018, 5:52:30 PM
 */
@Slf4j
@RequiredArgsConstructor
public class ApiHttpClient implements Client {

    @Nonnull
    private final FeignConfigInstance feignConfigInstance;

    private ApacheHttpClient delegate;

    private final Lock createClientLock = new ReentrantLock();

    @Override
    public Response execute(@Nonnull final Request request, @Nullable final Request.Options options) throws IOException {
        final Response response = getDelegate().execute(request, options);
        if (response.status() != 302) {
            return response;
        }

        log.info("Received 302 status at {}", request.requestTemplate().url());
        final var redirectStrategyConfig = feignConfigInstance.getRedirectStrategyConfig();
        if (!redirectStrategyConfig.isAllowRedirect()) {
            return response;
        }

        /**
         * Specially handle 302 in response in POST, since apache http client
         * redirects POST to GET.
         *
         * Allows only redirect to https
         */
        final String newUrl = emptyIfNull(response.headers().get("location"))
                .stream()
                .findFirst()
                .orElse(null);
        if (newUrl == null) {
            return response;
        }

        final boolean isSecureRedirect = newUrl.startsWith("https://");
        final boolean isRedirectAllowed = redirectStrategyConfig.isAllowInSecureRedirects() || isSecureRedirect;
        if (isRedirectAllowed) {
            final Request redirectRequest = Request.create(
                    request.httpMethod(),
                    newUrl,
                    request.headers(),
                    request.body(),
                    request.charset(),
                    request.requestTemplate());

            log.info("Redirect <{}> to: {}", request.url(), newUrl);

            return getDelegate().execute(redirectRequest, options);
        }

        return response;
    }

    @Nonnull
    private ApacheHttpClient getDelegate() {
        if (delegate != null) {
            return delegate;
        }

        try {
            createClientLock.lock();

            // After acquiring lock, be sure to check that the delegate has not been initialized.
            // This may happen during the wait for a lock.
            if (delegate != null) {
                return delegate;
            }

            final var connectionConfig = feignConfigInstance.getHttpConnectionConfig();
            final HttpClient httpClient = createHttpClient(connectionConfig);
            return delegate = new ApacheHttpClient(httpClient);
        } finally {
            try {
                createClientLock.unlock();
            } catch (final IllegalMonitorStateException ime) {
                log.warn("Lock was not held by current thread for delegate creation.", ime);
            }
        }
    }

    @Nonnull
    private HttpClient createHttpClient(@Nonnull final HttpConnectionConfig connectionConfig) {
        final HttpClientBuilder httpClientBuilder = HttpClients.custom();
        httpClientBuilder.setConnectionManager(createPooledConnection(connectionConfig));

        final RequestConfig requestConfig = createRequestConfig(connectionConfig);
        httpClientBuilder.setDefaultRequestConfig(requestConfig);

        return httpClientBuilder.build();
    }

    @Nonnull
    private HttpClientConnectionManager createPooledConnection(@Nonnull final HttpConnectionConfig connectionConfig) {
        final long timeToLive = connectionConfig.getTimeToLive();
        final PoolingHttpClientConnectionManager clientConnectionManager
                = new PoolingHttpClientConnectionManager(timeToLive, TimeUnit.SECONDS);
        clientConnectionManager.setMaxTotal(connectionConfig.getMaximumConnection());
        clientConnectionManager.setDefaultMaxPerRoute(connectionConfig.getMaximumConnectionPerRoute());

        return clientConnectionManager;
    }

    @Nonnull
    private RequestConfig createRequestConfig(@Nonnull final HttpConnectionConfig connectionConfig) {
        return RequestConfig.custom()
                .setContentCompressionEnabled(true)
                .setMaxRedirects(3)
                .setConnectTimeout(connectionConfig.getConnectionTimeout())
                .setConnectionRequestTimeout(connectionConfig.getConnectionTimeout())
                .setSocketTimeout(connectionConfig.getSocketTimeout())
                .build();
    }

}
