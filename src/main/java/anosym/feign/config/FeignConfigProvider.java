package anosym.feign.config;

import jakarta.annotation.Nonnull;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import java.util.Optional;
import org.eclipse.microprofile.config.Config;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 18, 2018, 1:01:00 PM
 */
@ApplicationScoped
public class FeignConfigProvider {

    private static final String PREFIX = "anosym.feign.%s.%s";

    @Inject
    private Instance<Config> configProperties;

    @Nonnull
    public FeignConfigInstance getFeignConfigInstance(@Nonnull String feignClientInstanceId) {
        final var config = this.configProperties.get();
        final var feignConfigInstanceBuilder = FeignConfigInstance.builder();
        config.getOptionalValue(PREFIX.formatted(feignClientInstanceId, "endpoint"), String.class)
                .ifPresent(feignConfigInstanceBuilder::endpoint);
        config.getOptionalValue(PREFIX.formatted(feignClientInstanceId, "httpsEnabled"), Boolean.class)
                .ifPresent(feignConfigInstanceBuilder::httpsEnabled);
        config.getOptionalValue(PREFIX.formatted(feignClientInstanceId, "active"), Boolean.class)
                .ifPresent(feignConfigInstanceBuilder::active);
        config.getOptionalValue(PREFIX.formatted(feignClientInstanceId, "httpConnectionConfig"), HttpConnectionConfig.class)
                .or(() -> Optional.of(new HttpConnectionConfig()))
                .ifPresent(feignConfigInstanceBuilder::httpConnectionConfig);
        config.getOptionalValue(PREFIX.formatted(feignClientInstanceId, "redirectStrategyConfig"), RedirectStrategyConfig.class)
                .or(() -> Optional.of(new RedirectStrategyConfig()))
                .ifPresent(feignConfigInstanceBuilder::redirectStrategyConfig);
        return feignConfigInstanceBuilder.build();
    }

}
