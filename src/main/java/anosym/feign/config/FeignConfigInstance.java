package anosym.feign.config;

import jakarta.annotation.Nonnull;
import lombok.Builder;
import lombok.Value;

/**
 *
 * @author ochieng-marembo
 */
@Value
@Builder
public class FeignConfigInstance {

    @Nonnull
    String endpoint;
    
    boolean httpsEnabled;
    
    boolean active;
    
    @Nonnull
    HttpConnectionConfig httpConnectionConfig;
    
    @Nonnull
    RedirectStrategyConfig redirectStrategyConfig;

}
