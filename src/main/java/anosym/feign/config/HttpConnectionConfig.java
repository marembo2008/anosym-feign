package anosym.feign.config;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 19, 2017, 8:48:07 PM
 */
@Data
@NoArgsConstructor
public class HttpConnectionConfig {

    private long timeToLive = 60;

    private int maximumConnection = 200;

    private int maximumConnectionPerRoute = 50;

    private int connectionTimeout = 100;

    private int socketTimeout = 1000;

}
