package anosym.feign.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 5, 2019, 1:08:46 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RedirectStrategyConfig {

    private boolean allowRedirect = true;

    private boolean allowInSecureRedirects = false;

}
