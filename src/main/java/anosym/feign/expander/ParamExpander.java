package anosym.feign.expander;

import feign.Param;
import org.atteo.classindex.IndexSubclasses;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 10, 2018, 10:14:51 AM
 */
@IndexSubclasses
public interface ParamExpander<T> extends Param.Expander {
}
