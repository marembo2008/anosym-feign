package anosym.feign;

import anosym.feign.config.FeignConfigInstance;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import anosym.feign.config.FeignConfigProvider;
import anosym.feign.interceptor.ApiRequestInterceptor;
import anosym.feign.interceptor.FeignInterceptor;
import anosym.feign.interceptor.RequestInterceptorWrapper;
import feign.DynamicExpanderContract;
import feign.Logger;
import feign.RequestTemplate;
import feign.Response;
import feign.codec.EncodeException;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import feign.form.FormEncoder;
import feign.jackson.JacksonDecoder;
import jakarta.ws.rs.NotAuthorizedException;
import jakarta.ws.rs.NotFoundException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.slf4j.MDC;

import feign.Feign;

import static java.lang.String.format;
import static java.util.stream.Collector.Characteristics.IDENTITY_FINISH;
import static java.util.stream.Collectors.groupingBy;

import static feign.FeignException.errorStatus;
import jakarta.annotation.Nonnull;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import java.util.Optional;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 18, 2018, 12:31:14 PM
 */
@Slf4j
@ApplicationScoped
public class FeignClientProvider {

    private static final String DEFAULT_FEIGN_INTERCEPTOR = "default";

    private final Map<String, List<ApiRequestInterceptor>> feignInterceptors;

    private final FeignConfigProvider feignConfigProvider;

    private final ObjectMapper objectMapper;

    private final Map<FeignClientContext, FeignClientTarget> feignClients = new ConcurrentHashMap<>();

    @Inject
    public FeignClientProvider(@Nonnull FeignConfigProvider feignConfigProvider,
            @Nonnull ObjectMapper objectMapper,
            @Nonnull Instance<ApiRequestInterceptor> feignInterceptors) {
        this.feignConfigProvider = feignConfigProvider;
        this.objectMapper = objectMapper;
        this.feignInterceptors = feignInterceptors.stream()
                .flatMap((interceptor) -> getFeignClients(interceptor).stream().map((fc) -> Pair.of(fc, interceptor)))
                .collect(groupingBy(Pair::getKey, toList(Pair::getValue)));

        log.info("feignInterceptors: {}", feignInterceptors);
    }

    @Nonnull
    public FeignClientTarget provide(@Nonnull final Class<?> feignClientInterface) {
        try {
            final FeignClient feignClient = getFeignClient(feignClientInterface);
            final FeignClientContext fcContext = new FeignClientContext(feignClient.value(), feignClientInterface);
            return feignClients.computeIfAbsent(fcContext,
                    (fcId) -> createFeignClient(feignClient, feignClientInterface));
        } catch (final Exception ex) {
            throw new FeignInitializationException("Failed to create feign client for: " + feignClientInterface, ex);
        }
    }

    @Nonnull
    private <T> FeignClient getFeignClient(@Nonnull final Class<T> feignClientInterface) {
        if (feignClientInterface.isAnnotationPresent(FeignClient.class)) {
            return feignClientInterface.getAnnotation(FeignClient.class);
        }

        final Class<?>[] superInterfaces = feignClientInterface.getInterfaces();
        if (superInterfaces.length > 0) {
            return getFeignClient(superInterfaces[0]);
        }

        throw new IllegalArgumentException(format("Invalid feign interface. Not annotated with @FeignClient: %s",
                feignClientInterface));
    }

    @Nonnull
    private FeignClientTarget createFeignClient(final FeignClient feignClient,
            final Class<?> feignClientInterface) {
        log.info("Creating feign clients: {}, {}", feignClient, feignClientInterface);

        final var fcInstanceId = feignClient.value();
        final var instance = feignConfigProvider.getFeignConfigInstance(fcInstanceId);
        final Object feignClientInstance
                = createFeignClient(fcInstanceId, feignClientInterface, instance);
        return FeignClientTarget.builder()
                .feignClientInstanceId(fcInstanceId)
                .feignClientInstance(feignClientInstance)
                .build();
    }

    @Nonnull
    private <T> T createFeignClient(@Nonnull final String feignClientInstanceId,
            @Nonnull final Class<T> feignClientInterface,
            @Nonnull final FeignConfigInstance feignConfigInstance) {
        final ApiUrlTarget<T> apiTarget = ApiUrlTarget
                .<T>builder()
                .feignClientInstance(feignConfigInstance)
                .feignInterface(feignClientInterface)
                .feignClientId(feignClientInstanceId)
                .build();

        log.info("Registering feign api for: {}", apiTarget);

        final Feign.Builder feignBuilder = Feign.builder();
        final List<ApiRequestInterceptor> fcInterceptors
                = feignInterceptors.getOrDefault(feignClientInstanceId, Collections.emptyList());

        log.debug("feign client instance interceptors <{}>: {}", feignClientInstanceId, fcInterceptors);

        final List<ApiRequestInterceptor> defaultInterceptors
                = feignInterceptors.getOrDefault(DEFAULT_FEIGN_INTERCEPTOR, Collections.emptyList());

        log.debug("feign client default interceptors: {}", defaultInterceptors);

        // if the feign group and instance ids are the same,
        // then we need to register only a single instance of the interceptor
        fcInterceptors.stream()
                .distinct()
                .map((interceptor) -> new RequestInterceptorWrapper(interceptor, feignConfigInstance))
                .forEach(feignBuilder::requestInterceptor);
        defaultInterceptors
                .stream()
                .distinct()
                .map((interceptor) -> new RequestInterceptorWrapper(interceptor, feignConfigInstance))
                .forEach(feignBuilder::requestInterceptor);

        final T feign = feignBuilder
                .encoder(new FormEncoder(new ActualTypeJacksonEncoder(objectMapper)))
                .decoder(new JacksonDecoder(objectMapper))
                .contract(new DynamicExpanderContract())
                .logLevel(Logger.Level.FULL)
                .logger(new FLogger(feignClientInterface))
                .client(new ApiHttpClient(feignConfigInstance))
                .errorDecoder(new FeignErrorDecoder())
                .target(apiTarget);

        return feign;
    }

    @Nonnull
    private List<String> getFeignClients(@Nonnull final ApiRequestInterceptor interceptor) {
        final var feignInterceptor = interceptor.getClass()
                .getAnnotation(FeignInterceptor.class);
        return Optional.of(feignInterceptor.value())
                .map(List::of)
                .orElse(List.of(DEFAULT_FEIGN_INTERCEPTOR));
    }

    @Data
    private static class Pair<K, V> {

        private final K key;

        private final V value;

        public static <K, V> Pair<K, V> of(@Nonnull final K key, @Nonnull final V value) {
            return new Pair<>(key, value);
        }

    }

    private static final class ActualTypeJacksonEncoder implements Encoder {

        private final ObjectMapper objectMapper;

        public ActualTypeJacksonEncoder(ObjectMapper objectMapper) {
            this.objectMapper = objectMapper;
        }

        @Override
        public void encode(@Nonnull final Object object,
                @Nonnull final Type bodyType,
                @Nonnull final RequestTemplate template) throws EncodeException {
            try {
                template.body(objectMapper.writeValueAsBytes(object), StandardCharsets.UTF_8);
            } catch (JsonProcessingException e) {
                throw new EncodeException(e.getMessage(), e);
            }
        }

    }

    private static class FeignErrorDecoder implements ErrorDecoder {

        @Override
        public Exception decode(@Nonnull final String methodKey, @Nonnull final Response response) {
            final int status = response.status();
            final jakarta.ws.rs.core.Response error = jakarta.ws.rs.core.Response.status(status)
                    .entity(format("%s: %s", methodKey, response.reason()))
                    .build();
            if (status == 404) {
                return new NotFoundException(error);
            } else if (status == 401 || status == 403) {
                return new NotAuthorizedException(error);
            }

            return errorStatus(methodKey, response);
        }

    }

    private static class FLogger extends feign.Logger {

        private final org.slf4j.Logger fLog;

        public FLogger(@Nonnull final Class<?> feignClientInterface) {
            this.fLog = org.slf4j.LoggerFactory.getLogger(feignClientInterface);
        }

        @Override
        protected void log(final String configKey, final String format, final Object... args) {
            MDC.put("feign-request", configKey);
            fLog.debug(String.format(format, args));
        }

    }

    @Nonnull
    private static <T, K> Collector<T, ?, List<K>> toList(@Nonnull final Function<T, K> mapper) {
        return Collector.of((Supplier<List<K>>) ArrayList::new,
                (list, value) -> list.add(mapper.apply(value)),
                (left, right) -> {
                    left.addAll(right);
                    return left;
                },
                IDENTITY_FINISH);
    }

}
