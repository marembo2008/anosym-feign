package anosym.feign;


import jakarta.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 5, 2018, 11:26:32 PM
 */
@Value
@Builder
@ToString(exclude = "feignClientInstance")
public class FeignClientTarget {

    @Nonnull
    String feignClientInstanceId;

    @Nonnull
    @JsonIgnore
    Object feignClientInstance;

}
