package anosym.feign;

import jakarta.annotation.Nonnull;

import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 30, 2018, 12:02:59 AM
 */
@Data
public class FeignClientId {

    @Nonnull
    private final String groupId;

    @Nonnull
    private final String instanceId;

}
