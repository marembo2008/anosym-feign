package anosym.feign;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Stereotype;
import jakarta.enterprise.util.Nonbinding;
import jakarta.inject.Named;
import org.atteo.classindex.IndexAnnotated;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 31, 2017, 10:49:45 AM
 */
@Named
@Inherited
@Stereotype
@IndexAnnotated
@ApplicationScoped
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
public @interface FeignClient {

  @Nonbinding
  String value();

}
