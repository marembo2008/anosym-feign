package anosym.feign;

import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 9, 2018, 10:23:39 PM
 */
public class FeignInitializationException extends RuntimeException {

    public FeignInitializationException(@NonNull final String message) {
        super(message);
    }

    public FeignInitializationException(@NonNull final String message, @NonNull final Throwable cause) {
        super(message, cause);
    }

}
