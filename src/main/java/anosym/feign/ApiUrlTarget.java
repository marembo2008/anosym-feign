package anosym.feign;

import anosym.feign.config.FeignConfigInstance;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jakarta.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.annotations.VisibleForTesting;

import feign.Request;
import feign.RequestTemplate;
import feign.Target;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static com.google.common.base.Preconditions.checkNotNull;
import static feign.Util.emptyToNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 6, 2019, 8:55:52 PM
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class ApiUrlTarget<T> implements Target<T> {

    private static final Pattern URL_WITH_PROTOCOL = Pattern.compile("(?<httpProtocol>http[s]?)?(://)?(?<serverAddress>.+)");

    @Nonnull
    private final FeignConfigInstance feignClientInstance;

    @Nonnull
    private final Class<T> feignInterface;

    @Nonnull
    private final String feignClientId;

    @Override
    public Class<T> type() {
        return feignInterface;
    }

    @Override
    public String name() {
        return feignClientId;
    }

    @Override
    public String url() {
        return getSanitizedUrl(feignClientInstance.getEndpoint());
    }

    @Override
    public Request apply(@NonNull final RequestTemplate input) {
        input.target(url());
        return input.request();
    }

    @Nonnull
    @VisibleForTesting
    String getSanitizedUrl(@Nonnull final String url) {
        checkNotNull(emptyToNull(url),
                "No url provided for feign api client <%s>, feignInterface <%s>",
                feignClientId,
                feignInterface);

        final Matcher urlMatcher = URL_WITH_PROTOCOL.matcher(url);

        log.debug("Sanitizing url <{}>. Matched: {}", url, urlMatcher.find());

        final String httpProtocol = Optional
                .ofNullable(urlMatcher.group("httpProtocol"))
                .filter((httpP) -> !httpP.trim().isEmpty())
                .orElse("http");
        final String serverAddress = urlMatcher.group("serverAddress");
        final boolean isSecure = httpProtocol.equals("https");
        if (isSecure) {
            return url;
        }

        final String requiredHttpProtocol = feignClientInstance.isHttpsEnabled()
                ? "https" : httpProtocol;
        return requiredHttpProtocol + "://" + serverAddress;
    }

}
