package anosym.feign.paging;

import java.util.List;

import jakarta.annotation.Nonnull;

import com.google.common.base.Splitter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import static java.lang.String.format;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 9, 2018, 10:28:04 PM
 */
@Data
@Builder
@Setter(AccessLevel.NONE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OffsetPageInfo implements PageInfo {

    private static final Splitter HASH = Splitter.on("#").trimResults().omitEmptyStrings();

    @Nonnull
    @Builder.Default
    private Integer pageNumber = 0;

    @Nonnull
    @Builder.Default
    private Integer pageSize = 100;

    @Nonnull
    @Builder.Default
    private Integer pageCount = -1;

    @Override
    public String getNextPage() {
        final int nextPage = pageNumber + 1;
        if (pageCount > -1 && nextPage == pageCount) {
            throw new PageOutOfRangeException("Next page out of range page<%s> and pageCount <%s>", nextPage, pageCount);
        }

        return format("%s#%s#%s", nextPage, pageSize, pageCount);
    }

    @NonNull
    public static OffsetPageInfo fromString(@NonNull final String pageInfo) {
        final List<String> parts = HASH.splitToList(pageInfo);
        return OffsetPageInfo.builder()
                .pageNumber(Integer.parseInt(parts.get(0)))
                .pageSize(Integer.parseInt(parts.get(1)))
                .pageCount(((parts.size() == 3) ? Integer.parseInt(parts.get(2)) : -1))
                .build();
    }

}
