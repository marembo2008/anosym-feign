package anosym.feign.paging;

import jakarta.annotation.Nullable;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.Setter;

import static java.lang.String.format;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 9, 2018, 10:30:21 PM
 */
@Data
@Builder
@Setter(AccessLevel.NONE)
public class PagingStateInfo implements PageInfo {

    @Nullable
    private String pagingState;

    @NonNull
    private Integer pageSize;

    @Override
    public String getNextPage() {
        return format("%s#%s", pagingState, pageSize);
    }

    @NonNull
    public static PagingStateInfo fromString(@NonNull final String pageInfo) {
        final String[] parts = pageInfo.split("#");
        return PagingStateInfo.builder()
                .pagingState(parts[0])
                .pageSize(Integer.parseInt(parts[1]))
                .build();
    }

}
