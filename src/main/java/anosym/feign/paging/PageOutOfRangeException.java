package anosym.feign.paging;

import lombok.NonNull;

import static java.lang.String.format;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 9, 2018, 10:53:27 PM
 */
public class PageOutOfRangeException extends RuntimeException {

    private final int page;

    private final int pageCount;

    public PageOutOfRangeException(@NonNull final String messageTemplate, final int page, final int pageCount) {
        super(messageTemplate);
        this.page = page;
        this.pageCount = pageCount;
    }

    @Override
    public String getMessage() {
        return format(getMessage(), page, pageCount);
    }

}
