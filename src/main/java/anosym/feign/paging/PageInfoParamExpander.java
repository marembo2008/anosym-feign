package anosym.feign.paging;

import lombok.NonNull;

import static com.google.common.base.Preconditions.checkArgument;

import anosym.feign.expander.ParamExpander;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 10, 2018, 9:39:06 AM
 */
public class PageInfoParamExpander implements ParamExpander<PageInfo> {

    @Override
    public String expand(@NonNull final Object value) {
        checkArgument(value instanceof PageInfo, "Can only expand an instance of PageInfo");

        return ((PageInfo) value).getNextPage();
    }

}
