package anosym.feign.paging;

import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 9, 2018, 10:26:49 PM
 */
public interface PageInfo {

    @NonNull
    String getNextPage();

}
