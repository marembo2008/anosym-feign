package anosym.feign.paging;

import java.util.List;

import jakarta.annotation.Nonnull;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 25, 2018, 10:30:33 PM
 */
@Data
@Builder
@Setter(AccessLevel.NONE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public final class Page<T, PI extends PageInfo> {

    @Nonnull
    private PI pageInfo;

    @Nonnull
    @Singular
    private List<T> contents;

}
