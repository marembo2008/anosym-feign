package anosym.feign.paging;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import jakarta.annotation.Nullable;

import jakarta.ws.rs.ext.ParamConverter;
import jakarta.ws.rs.ext.ParamConverterProvider;
import jakarta.ws.rs.ext.Provider;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 10, 2018, 12:28:47 PM
 */
@Provider
public class PageInfoParamConverterProvider implements ParamConverterProvider {

  private final ParamConverter<OffsetPageInfo> offsetPageInfoConverter = new OffsetPageInfoParamConverter();

  private final ParamConverter<PagingStateInfo> pagingStateInfoConverter = new PagingStateInfoParamConverter();

  @Override
  public <T> ParamConverter<T> getConverter(@NonNull final Class<T> type,
                                            @NonNull final Type type1,
                                            @NonNull final Annotation[] antns) {
    if (type == OffsetPageInfo.class) {
      return (ParamConverter<T>) offsetPageInfoConverter;
    }

    if (type == PagingStateInfo.class) {
      return (ParamConverter<T>) pagingStateInfoConverter;
    }

    return null;
  }

  private static final class OffsetPageInfoParamConverter implements ParamConverter<OffsetPageInfo> {

    @Override
    public OffsetPageInfo fromString(@Nullable final String pageInfo) {
      if (pageInfo == null) {
        return null;
      }

      return OffsetPageInfo.fromString(pageInfo);
    }

    @Override
    public String toString(@Nullable final OffsetPageInfo pageInfo) {
      if (pageInfo == null) {
        return null;
      }

      return pageInfo.getNextPage();
    }

  }

  private static final class PagingStateInfoParamConverter implements ParamConverter<PagingStateInfo> {

    @Override
    public PagingStateInfo fromString(@Nullable final String pageInfo) {
      if (pageInfo == null) {
        return null;
      }

      return PagingStateInfo.fromString(pageInfo);
    }

    @Override
    public String toString(@Nullable final PagingStateInfo pageInfo) {
      if (pageInfo == null) {
        return null;
      }

      return pageInfo.getNextPage();
    }

  }

}
