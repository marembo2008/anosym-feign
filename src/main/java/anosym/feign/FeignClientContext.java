package anosym.feign;

import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 9, 2018, 10:26:17 PM
 */
@Data
final class FeignClientContext {

    @NonNull
    private final String fcId;

    @NonNull
    private final Class<?> fcInterface;

}
