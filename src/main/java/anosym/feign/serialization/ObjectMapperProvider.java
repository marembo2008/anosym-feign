package anosym.feign.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import org.atteo.classindex.IndexSubclasses;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 20, 2018, 4:33:09 AM
 */
@IndexSubclasses
public interface ObjectMapperProvider {

    @NonNull
    ObjectMapper provide();

}
