package anosym.feign.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;

import anosym.json.serialization.provider.ObjectMapperFactory;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 23, 2018, 9:29:09 PM
 */
public class DefaultObjectMapperProvider implements ObjectMapperProvider {

    @Override
    public ObjectMapper provide() {
        return ObjectMapperFactory.getObjectMapper();
    }

}
