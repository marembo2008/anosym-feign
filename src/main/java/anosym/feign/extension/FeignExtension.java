package anosym.feign.extension;

import anosym.feign.FeignClient;
import java.util.Set;

import jakarta.annotation.Nonnull;

import anosym.feign.FeignClientProvider;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.inject.spi.AfterBeanDiscovery;
import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.enterprise.inject.spi.Extension;
import java.util.UUID;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.joining;
import java.util.stream.StreamSupport;
import lombok.extern.slf4j.Slf4j;
import static org.atteo.classindex.ClassIndex.getAnnotated;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 2, 2017, 9:51:05 PM
 */
@Slf4j
public class FeignExtension implements Extension {

    private static final Set<Class<?>> FEIGN_CLIENTS = getFeignClients();

    @SuppressWarnings("unused")
    void afterBeanDiscovery(@Observes AfterBeanDiscovery afterBeanDiscovery,
            @Nonnull BeanManager beanManager) {
        final Bean<Object> fcProvider = (Bean<Object>) beanManager
                .getBeans(FeignClientProvider.class)
                .iterator()
                .next();
        FEIGN_CLIENTS
                .forEach((fc) -> createFeignClientBeans(afterBeanDiscovery, fcProvider, fc));
    }

    @Nonnull
    private void createFeignClientBeans(
            @Nonnull AfterBeanDiscovery afterBeanDiscovery,
            @Nonnull Bean<Object> feignClientProviderBean,
            @Nonnull Class<?> feignClient) {
        afterBeanDiscovery.addBean()
                .types(feignClient, Object.class)
                .name(getBeanName(feignClient))
                .id("%s-%s".formatted(feignClient.getCanonicalName(), UUID.randomUUID()))
                .qualifiers(new AnyAnnotation(), new DefaultAnnotation())
                .scope(ApplicationScoped.class)
                .stereotypes(Set.of(FeignClient.class))
                .alternative(false)
                .createWith(context -> {
                    final FeignClientProvider feignClientProvider
                            = (FeignClientProvider) feignClientProviderBean.create(context);
                    final var feignClientTarget =  feignClientProvider.provide(feignClient);
                    return feignClientTarget.getFeignClientInstance();
                });
    }

    private String getBeanName(@Nonnull Class<?> feignClient) {
        final var simpleName = feignClient.getSimpleName();
        return "%s%s".formatted(simpleName.substring(0, 1).toLowerCase(), simpleName.substring(1));
    }

    @Nonnull
    private static Set<Class<?>> getFeignClients() {
        final var classLoader = FeignExtension.class.getClassLoader();
        final Set<Class<?>> feignClients = StreamSupport
                .stream(getAnnotated(FeignClient.class, classLoader).spliterator(), false)
                // CDI may use decorators that implement the apis. Remove them from the list of
                // feign clients
                .filter(Class::isInterface)
                .sorted((o1, o2) -> o1.getCanonicalName().compareTo(o2.getCanonicalName()))
                .collect(Collectors.toSet());
        final var debugFeignClients = feignClients
                .stream()
                .map(Class::getCanonicalName)
                .collect(joining("\n---", "\n\n---", "\n\n"));
        log.info("Found feign clients: {}", debugFeignClients);

        return feignClients;
    }
}
