package anosym.feign.extension;

import jakarta.enterprise.inject.Any;
import jakarta.enterprise.util.AnnotationLiteral;

@SuppressWarnings(value = "AnnotationAsSuperInterface")
final class AnyAnnotation extends AnnotationLiteral<Any> implements Any {

  private static final long serialVersionUID = -1485969418517333998L;

}
