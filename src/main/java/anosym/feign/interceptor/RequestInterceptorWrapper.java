package anosym.feign.interceptor;

import anosym.feign.config.FeignConfigInstance;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.AllArgsConstructor;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 29, 2018, 11:58:11 PM
 */
@AllArgsConstructor
public class RequestInterceptorWrapper implements RequestInterceptor {

    @NonNull
    private final ApiRequestInterceptor delegatedFeignInterceptor;

    @NonNull
    private final FeignConfigInstance feignConfigInstance;

    @Override
    public void apply(@NonNull final RequestTemplate template) {
        delegatedFeignInterceptor.apply(feignConfigInstance, template);
    }

}
