package anosym.feign.interceptor;

import anosym.feign.config.FeignConfigInstance;
import feign.RequestTemplate;
import jakarta.annotation.Nonnull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 30, 2018, 12:02:27 AM
 */
public interface ApiRequestInterceptor {

    void apply(@Nonnull FeignConfigInstance feignConfigInstance, @Nonnull RequestTemplate requestTemplate);

}
