package anosym.feign.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.atteo.classindex.IndexAnnotated;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 31, 2017, 10:49:45 AM
 */
@Inherited
@IndexAnnotated
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FeignInterceptor {

    /**
     * The name of the feign client, which the interceptor is responsible for.
     */
    String[] value() default {};

}
