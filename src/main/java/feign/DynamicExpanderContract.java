package feign;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import jakarta.annotation.Nullable;

import com.google.common.collect.ImmutableList;

import anosym.feign.expander.ParamExpander;
import feign.Param.Expander;
import lombok.NonNull;

import static feign.Util.checkState;
import static feign.Util.emptyToNull;
import static org.atteo.classindex.ClassIndex.getSubclasses;

/**
 * This is a copy of {@link Contract.Default} to add away to dynamically specify
 * param expanders.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 10, 2018, 9:51:04 AM
 */
public class DynamicExpanderContract extends Contract.Default {

    private static final Map<Class<?>, Class<? extends Expander>> EXPANDERS;

    static {
        final var classLoader = DynamicExpanderContract.class.getClassLoader();
        EXPANDERS = ImmutableList.copyOf(getSubclasses(ParamExpander.class, classLoader))
                .stream()
                .collect(Collectors.toMap(DynamicExpanderContract::getSupportedType, Function.identity()));
    }

    public DynamicExpanderContract() {
        super.registerParameterAnnotation(Param.class, (paramAnnotation, data, paramIndex) -> {
            final String annotationName = paramAnnotation.value();
            final Parameter parameter = data.method().getParameters()[paramIndex];
            final String name;
            if (emptyToNull(annotationName) == null && parameter.isNamePresent()) {
                name = parameter.getName();
            } else {
                name = annotationName;
            }
            checkState(emptyToNull(name) != null, "Param annotation was empty on param %s.",
                    paramIndex);
            nameParam(data, name, paramIndex);
            final Class<? extends Param.Expander> expander = paramAnnotation.expander();
            if (expander != Param.ToStringExpander.class) {
                data.indexToExpanderClass().put(paramIndex, expander);
            } else {
                final Class<?> paramType = parameter.getType();
                final Class<? extends Param.Expander> paramExpander = EXPANDERS
                        .entrySet()
                        .stream()
                        .filter((expanderCls) -> expanderCls.getKey().isAssignableFrom(paramType))
                        .sorted((cl0, cl1)
                                -> Boolean.compare(cl0.getKey() == paramType, cl1.getKey() == paramType))
                        .findFirst()
                        .map(Entry::getValue)
                        .orElse(null);
                if (paramExpander != null) {
                    data.indexToExpanderClass().put(paramIndex, paramExpander);
                }
            }
            if (!data.template().hasRequestVariable(name)) {
                data.formParams().add(name);
            }
        });
    }

    @NonNull
    private static Class<?> getSupportedType(@NonNull final Class<?> paramExpander) {
        final Type genericType = paramExpander.getGenericSuperclass();
        if (genericType instanceof ParameterizedType parameterizedType) {
            return (Class<?>) parameterizedType.getActualTypeArguments()[0];
        }

        final ParameterizedType interfaceType = getGenericSuperInterface(paramExpander);
        checkState(interfaceType != null,
                "Param expander must be declared with a concrete generic type: %s",
                paramExpander);

        return (Class<?>) interfaceType.getActualTypeArguments()[0];
    }

    @Nullable
    private static ParameterizedType getGenericSuperInterface(@NonNull final Class<?> paramExpander) {
        final Type[] types = paramExpander.getGenericInterfaces();
        if (types == null || types.length == 0) {
            return null;
        }

        return Arrays.stream(types)
                .filter(ParameterizedType.class::isInstance)
                .map(ParameterizedType.class::cast)
                .findFirst()
                .orElse(null);
    }

}
